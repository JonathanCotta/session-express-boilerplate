const express = require("express");
const morgan = require("morgan");
const session = require("express-session")
const mongoStore = require("connect-mongodb-session")(session);

const config = require("./config");
const router = require("./routes");
const db = require("./database").connection;

const app = express();
const PORT = process.env.PORT || 8888;
const store = new mongoStore({ uri: db.connection, collection: 'sessions' });

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(session({
    secret: config.secret,
    resave: false,
    saveUninitialized: true,
    cookie: { 
        secure: false,
        maxAge: 3600*1000
    },
    store: store
}));

app.use(morgan("dev"));

app.use("/service/", router);

app.listen(PORT, () => console.log(`server is runing on http://localhost:${PORT}/api/`));