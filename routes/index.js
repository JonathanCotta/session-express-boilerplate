const router = require("express").Router();


const userCtlr = require("../controllers/userController");
const auth = require("../middleware/auth");

router.get("/users", auth("admin"), userCtlr.getAll);
router.get("/user/:id", auth(), userCtlr.getOne);
router.post("/user/", auth(), userCtlr.create);
router.put("/user/", auth(), userCtlr.update);
router.delete("/user/:id", auth(), userCtlr.delete);

router.post("/login", userCtlr.login);
router.get("/logout", userCtlr.logout);
router.get("/session", userCtlr.sessionInfo);

module.exports = router;